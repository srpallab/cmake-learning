#include <iostream>
#include <string>
#include <GLFW/glfw3.h>
#include <HELLOConfig.h>
#include <QCoreApplication>

#ifdef USE_ADDER
#include <adder.h>
#endif // USE_ADDER


void do_cpp(){
  std::string name;
  std::cout << "Please Enter Your Name: ";
  std::getline(std::cin,name);
  std::cout << "Hello " << name << "\n";
}

int main(int argc, char* argv[]){
  std::cout << "Hello World\n";

  do_cpp();

  #ifdef USE_ADDER
  std::cout << adder(23.43f, 45.66f) << "\n";
  #else
  std::cout << "Adder library is turned off\n";
  #endif // USE_ADDER

  std::cout << argv[0] << " VERSION " << HELLO_VERSION_MAJOR << "." << HELLO_VERSION_MINOR << "\n";
  
  GLFWwindow *window;

  if (!glfwInit()) {
    fprintf(stderr, "Failed to initialize GLFW\n");
    exit(EXIT_FAILURE);
  }

  window = glfwCreateWindow(300, 300, "Gears", NULL, NULL);
  if (!window) {
    fprintf(stderr, "Failed to open GLFW window\n");
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  while (!glfwWindowShouldClose(window)) {
    glfwSwapBuffers(window);
    glfwPollEvents();
  }
  glfwTerminate();
  return 0;
}
